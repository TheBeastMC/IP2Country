//compile with: g++ IpToCountryCompressor -o IpToCountryCompressor
#include <stdio.h> //for printf
#include <string> //for strings
#include <fstream> //for reading & writing to a file
#include <algorithm> //for erase
#include <arpa/inet.h> //for inet_pton
//compare performance: map vs unordered_map vs vector vs unordered_vector vs list
#include <map> //for maps
using namespace std;

int main() {
	string line;
	ifstream input;
	input.open("IpToCountry.csv");
	ofstream output;
	output.open("IpToCountryCode.txt");
	if (input.is_open() && output.is_open()) {
		printf("Compressing ...\n");
		string endIp;
		string countryCode;
		string lastEndIp;
		string lastCountryCode = "ZZ";
		while (getline(input,line)) {
			//additional feature for better compression: we can skip lines that point to the same countryCode and come right next after each other by using this logic, so check if the last countryCode differs from the current one ///if the this countryCode is the same as the last countryCode, overwrite the last written line
			if (line[0] != ' ' && line[0] != '#') { //process only lines that dont start with a " " or #
				line.erase(remove(line.begin(), line.end(), '"'), line.end()); //remove all "
				line.erase(0, line.find(',')+1); //erase until first ,
				endIp = line.substr(0, line.find(',')); //endIp is the second parameter (split by ,)
				line.erase(0, line.find(',')+1); //erase until second ,
				line.erase(0, line.find(',')+1); //erase until third ,
				line.erase(0, line.find(',')+1); //erase until fourth ,
				countryCode = line.substr(0, line.find(',')); //countryCode is the fifth parameter (split by ,)
				//check before writing to file
				if (countryCode != lastCountryCode) {
					output << lastEndIp << "," << lastCountryCode << "\n";
				}
				lastEndIp = endIp;
				lastCountryCode = countryCode;
				//printf("Ip until %s is %s\n", endIp.c_str(), countryCode.c_str());
				//return 0;
			}
		}
		//and write the last one
		output << lastEndIp << "," << lastCountryCode;
		//and close everything
		input.close();
		output.close();
		//done
		printf("Done.\n");
	} else {
		printf("Unable to open input or output file.");
	}
	
	//read the above generated format the following (logic):
	//(last decimal ip),(2 letter iso country code)
	//first line is from zero to the number
	//second line is from the number from the last line + 1 until this line's number
	
	/*map<unsigned int, string> IpToCountryCode;
	unsigned short firsttime = 1;
	char ip[17];
	struct sockaddr_in sa;
	unsigned int foundinblock;
	while (1) {
		printf("Do you want me to get the Country by IP?\nJust enter it here in format XXX.XXX.XXX.XXX:\n");
		scanf("%s", ip);
		//convert ip from dotted notation to decimal
		inet_pton(AF_INET, ip, &(sa.sin_addr));
		if (firsttime == 1) {
			//import IpToCountryCode.txt
			input.open("IpToCountryCode.txt");
			if (input.is_open()) {
				while (getline(input,line)) {
					string endIpStr = line.substr(0, line.find(','));
					unsigned int endIp = atoi(endIpStr.c_str());
					line.erase(0, line.find(',')+1);
					string countryCode = line.substr(0, line.find(','));
					IpToCountryCode[endIp] = countryCode;
				}
				input.close();
			} else {
				printf("Unable to open IpToCountryCode.txt file.");
				return -1;
			}
			firsttime = 0;
		}
		//search for ip
		string client_country;
		unsigned int clientIp = (sa.sin_addr.s_addr>>24) | ((sa.sin_addr.s_addr>>8) & 0x0000ff00) | ((sa.sin_addr.s_addr<<8) & 0x00ff0000) | (sa.sin_addr.s_addr<<24);
		for (map<unsigned int, string>::iterator it2 = IpToCountryCode.begin(); it2 != IpToCountryCode.end(); ++it2) {
			if (clientIp <= it2->first) {
				client_country = it2->second;
				foundinblock = it2->first;
				break;
			}
		}
		printf("\nIP %s is %i in decimal and belongs to country %s, found in block %i\n", ip, clientIp, client_country.c_str(), foundinblock);
		printf("\nTo exit, just press CTRL + C\n\n");
	}*/
	return 0;
}