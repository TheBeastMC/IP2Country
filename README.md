# IP2Country

Decimal IP (range) to 2 letter Country Code

Based on the large IP DB from http://software77.net/geo-ip/

## Logic

1. Remove every line starting with "#" or " "
2. Remove every '"' in a line
3. Remove everything before the first "," (including the ",")
4. Remove everything after the second "," until the fourth "," (including the fourth ",")
5. Remove everything after the fifth "," (including the ",")
6. We can skip this line if the next line has the same Country Code as this one

### Example

Original:

```
#comment
0,"16777215","iana","410227200","ZZ","ZZZ","Reserved"
16777216,"16777471","apnic","1313020800","AU","AUS","Australia"
16777472,"16777727","apnic","1302739200","CN","CHN","China"
16777728,"16778239","apnic","1302739200","CN","CHN","China"
16778240,"16779263","apnic","1302566400","AU","AUS","Australia"
```

Becomes to:

```
16777215,ZZ
16777471,AU
16778239,CN
16779263,AU
```

As we don't need all the other info from the original, we can remove them and save around 80-90% of space.

## Reading the file (Pseudocode)

```
unsigned int decIP = decimal representation of the IPv4 address

for each line do {
    unsigned int lineNumber = the number before the ","
    char [2] countryCode = the 2 letters after the ","
    if (decIP <= lineNumber) {
        countryCode is the correct Country Code of this IP
    }
}
```